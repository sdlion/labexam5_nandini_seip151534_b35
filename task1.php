<?php
class array_sort
{
    protected $_asort;
    public function  __construct(array $asort)
    {
        $this ->_asort=$asort;
    }
    public function mySort(){
        $sorted_array=$this->_asort;
        sort($sorted_array);
        return $sorted_array;
    }
}
$sortarray = new array_sort(array(11, -2, 4, 35, 0, 8, -9));

print_r($sortarray->mySort())."\n";

?>