<?php
class MyCalculator {
    public $num1, $num2;
    public function __construct( $num1, $num2 ) {
        $this->num1 = $num1;
        $this->num2 = $num2;
    }
    public function add() {
        return $this->num1 + $this->num2;
    }
    public function subtract() {
        return $this->num2 - $this->num2;
    }
    public function multiply() {
        return $this->num1 * $this->num2;
    }
    public function divide() {
        return $this->num1 / $this->num2;
    }
}
$mycalc = new MyCalculator(12, 6);
echo "add ".$mycalc-> add();
echo "<br>";
echo "mul ".$mycalc-> multiply();
echo "<br>";
echo "sub ".$mycalc-> subtract();
echo "<br>";
echo "div ".$mycalc-> divide();
?>