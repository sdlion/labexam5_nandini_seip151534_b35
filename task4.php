<?php
class factorial_of_a_number
{
    public $number;
    public function __construct($number)
    {
        if (!is_int($number))
        {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->number = $number;
    }
    public function result()
    {
        $fact = 1;
        for ($i = 1; $i <= $this->number; $i++)
        {
            $fact *= $i;
        }
        return $fact;
    }
}

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
?>