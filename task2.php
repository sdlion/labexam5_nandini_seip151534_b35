<?php
$start_date=new datetime("1981-11-03");
$end_date=new datetime("2013-09-04");
$interval=$start_date->diff($end_date);
echo "Difference between two dates : " . $interval->y . " years, " . $interval->m." months, ".$interval->d." days "."\n";
